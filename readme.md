![Revolt](images/thorium.png)

## Installing Thorium
Download and install the Thorium package on the linux host (e.g., Raspberry Pi)
```
$ wget https://gitlab.com/SpuQ/thorium/repository/archive.tar?ref=master -O thorium.tar
$ tar -xf thorium.tar && cd thorium*
$ sudo ./install.sh
$ sudo shutdown -r now
```


After rebooting, the Thorium host system will be configured as wireless access point (e.g. thorium-xxxx). Connect to this network and
in your favorite browser go to http://10.30.40.1:3000. Now you should see something like this:
  
![ConfigUI](images/thorium_ui.png)

## License
This software is free for personal use. For commercial use, contact the author.
