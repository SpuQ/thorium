#!/bin/sh

echo "Backing up Revolt";
echo "removing old archive from local directory"
rm ${PWD##*/}*

NOW=$(date +"%Y%m%d-%H%M")
ARCHIVE_NAME=${PWD##*/}"_""$NOW""_""$1";

echo "creating archive $ARCHIVE_NAME";
#tar cvf $ARCHIVE_NAME.tar .
tar -cf $ARCHIVE_NAME.tar .

cp $ARCHIVE_NAME.tar ..
echo "done"
