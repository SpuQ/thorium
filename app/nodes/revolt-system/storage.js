/*
 *	storage.js
 *	object of persistent storage of variables
 */

var fs = require('fs');
var storagepath = "/var/revolt/app-vars/";

module.exports = Storage;

function Storage( name ){
	var name = name;
	
	// initialize
	this.init = function(){
		// check if file exists - create it if not
		if( !fs.existsSync(storagepath+name) ){
			var fd = fs.openSync(storagepath+name, 'w+');
			fs.closeSync(fd);
		}
	}

	// read from storage file
	this.read = function(){
		var content = fs.readFileSync(storagepath+name, 'ascii');

		// check for object
		try {
			var json = JSON.parse(content);
			return json;
		} catch(e){
			console.log("not a json object");
		}

		return content;
	}

	// write to storage file
	this.write = function( data ){
		// check for JSON object
		if( typeof(data) == "object" ){
			fs.writeFileSync(storagepath+name, JSON.stringify(data) );
		}
		else{
			fs.writeFileSync(storagepath+name, data);
		}
		return 0;
	}
}
