/*
 *	QcomV2.js
 *	NodeJS driver for QcomV2 (QdriverV2)
 *	based on QcomV1 driver (qcom.js)
 *
 *	written by:	Tom 'SpuQ' Santens
 *		on:	27/09/2016
 *
 *	helpful resources: https://nodejs.org/api/net.html
 * usage:
 *	var sensor = new Qdevice("es_lightsensor_1", function( signal ) {
 *						...
 *					});
 */

var net = require('net');
var fs = require('fs');
var readline = require('readline');

var socketDir = "/Qdev/"		// changed on 10/12/2016

module.exports = Qdevice;
module.exports.deviceList = getDeviceList();

// Qdevice object constructor
function Qdevice( name , handler ) {
	var reconnecting;
	var deviceName = name;
	var socketPath;
	var socketLineIn;
	var socketClient;

	var inputHandler = handler;

	console.log("constructing Qdevice for '"+deviceName+"'");

	socketPath = socketDir+deviceName;
	console.log(socketPath);

	connect();

	function connect(){
		reconnecting = false;
		socketClient = net.connect(socketPath);
		socketLineIn = readline.createInterface( { terminal : false, input: socketClient } );

		socketLineIn.on('line', function(line){
			//console.log("--> "+deviceName+": "+ line);

			var obj = JSON.parse(line);
			if(typeof(inputHandler) == "function"){		// check whether function is set
				inputHandler( obj );
			}
		});

		socketClient.on("error", function(err){
			if(err.code == "ECONNREFUSED"){
				//console.log(deviceName+": connection refused");
			}
		});

		socketClient.on("close", function(){
			//console.log(deviceName+": connection closed by server");
			reconnect();
		});
	}
	
	this.send = function( jsonmsg ) {
		if( jsonmsg.signal && jsonmsg.argument ) {
			var obj = {};
			obj.signal = jsonmsg.signal;
			obj.argument = jsonmsg.argument;
	
			if( !socketClient.write( JSON.stringify(obj), function(){/*console.log("<-- "+deviceName+": "+JSON.stringify(obj));*/} ) ) {
				//console.log(deviceName+" data constipation?");
			}
		}
	}

	this.disconnect = function(callback){
		socketClient.end();
		socketClient.unref();
		socketClient.destroy();
		console.log(deviceName+" disconnected");
		callback();
	}
	
	function reconnect(){
		if(reconnecting == false){
			reconnecting = true;
			//console.log(deviceName+": attempting to reconnect");
			setTimeout(connect, 1000);
		}
		else {
			console.log(deviceName+": attempts to reconnect are already being made");
		}
	}
}

// get the device list
function getDeviceList(){
	devices = {};
	devices = fs.readdirSync(socketDir);
	return devices;
}

