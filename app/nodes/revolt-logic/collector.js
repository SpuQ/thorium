module.exports = function(RED) {
	function collector(config) {
		RED.nodes.createNode(this, config);
		var node = this;
		var signal = config.signal;
		var keepTime = + config.keepTime;
		var sampleRate = + config.sampleRate;

		if( isNaN(sampleRate) ){
			sampleRate = 1000;
		}

		if( isNaN(keepTime) ){
			keepTime = 10000;
		}

		console.log("creating collection for '"+signal+"'")
		console.log("sample rate: "+sampleRate+"ms , keeptime: "+keepTime+"ms");

		var data = JSON.parse('{ "signal":"'+signal+'", "argument":[] }');

		var collectordata = {};

		this.on('input', function(msg) {
			if(msg.signal == signal){
				var timestamp = new Date().getTime();
				var newData = {};
				newData.argument = msg.argument;
				newData.timestamp = timestamp;
				
				collectordata = newData;
			}
		});

		setInterval( function(){
			data.argument.push(collectordata);	// put data into array

			// clean out old data
			var timestamp = new Date().getTime(); 	// unix date - miliseconds since epoc

			for (var i = 0, len = data.argument.length; i < len; i++) {
				//console.log(i+") "+data.argument[i].timestamp+": "+data.argument[i].argument);
				if( data.argument[i].timestamp < timestamp - keepTime){
					data.argument.splice(i,1);
					len--;
					i--;
				}
			}

			node.send(data);			// send data
		}, sampleRate );
	}

	RED.nodes.registerType("collector", collector);
}
