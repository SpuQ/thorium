module.exports = function(RED) {
	console.log('registering Experimental "speech" node');

	Speech = require('./speech');

	function speech_node(config){
        	RED.nodes.createNode(this, config);
		var node = this;
		var topic = config.topic;

		var speech = new Speech();

		// function called on incoming msg
		this.on('input', function( msg ) {
			if(msg.topic == topic){
				var err;
				status_indicator("speaking");
				err = speech.say( msg.payload );
				status_indicator("ok");
			}
		});

		// function called on application close
		this.on('close', function(done) {
			//TODO force off
			done();
		});

		status_indicator("error");	// set error by default

		// Status indicator beneath the node
		function status_indicator(status){
			if (status == "ok"){
				node.status({fill:"blue",shape:"dot",text:status});
			}
			else if(status == "speaking"){
				node.status({fill:"blue",shape:"dot",text:status});
			}
			else {
				node.status({fill:"red",shape:"dot",text:status});
			}
		}
	}

	RED.nodes.registerType("speech", speech_node);
	console.log('Experimental "speech" node registered');
}
