/*
 *	system.js
 *	NodeJS application for Revolt system
 *
 *	written by Tom 'SpuQ' Santens
 *		on 28/09/2016 - last edit: 19/11/2016
 */

var scriptsPath = "./scripts/";			// path to system scripts

/*	change working directory to app directory	*/
try {
	process.chdir('/opt/revolt/config/');	// changed 10/12/2016
}
catch (err) {
	console.log('chdir: ' + err);
}

var fs = require('fs');				// filesystem

var express = require('express');		// webserver
var app = express();
var path = require('path');
var bodyParser = require('body-parser');

var formidable = require("formidable");		// processing forms
var util = require('util');

var exec = require('child_process').exec;

app.use( express.static( path.join(__dirname+'/webui/')) );
app.use(bodyParser.json()); 					// support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true }));		// support encoded bodies

app.listen(3000, function () {
	console.log('Revolt configuration UI server listening on port 3000!');
});

app.get('/', function (req, res) {
	res.sendFile('index.html' );
});


/* 	include modules 	*/
require('./networking')(app);

var system = require('./revolt-system.js');

/* 
 *	Devices
 */
var QdeviceDir = '/Qdev/';		// directory with Qcom sockets

app.get('/api/qdevice-list', function (req, res) {
	var devices = fs.readdirSync(QdeviceDir);
	var list = [];

	for(var key in devices) {
		list[key] = {};
		list[key].device = devices[key];
	}

	res.setHeader('Content-Type', 'application/json');
	res.send( JSON.stringify(list) );
});


/*
 *	Application
 */

// AppUI upload TODO improve! 
app.post('/api/uploadWebui',function(req, res){
	console.log("incoming webUI package");
	var fields = [];
	var form = new formidable.IncomingForm();
 
	form.on('file', function (name, file) {
		//console.log(file.path);
		fields[name] = file;
		exec('sh ./scripts/deploy_appui.sh '+file.path);
	});

	form.on('end', function () {
		res.writeHead(200, {'content-type': 'text/plain'});
		res.end('{"status" : 200}');
	});
	form.parse(req);
});

// restart application
app.get('/api/restart_app', function (req, res) {
	console.log("request for application restart");
	res.setHeader('Content-Type', 'application/json');
	exec('sh ./scripts/restart_app.sh');
	res.send({'application':'restart'});
});

// delete program
app.get('/api/program_delete', function (req, res) {
	res.setHeader('Content-Type', 'application/json');
	res.send({'program':'delete'});
	exec('sh ./scripts/delete_program.sh');
});

/*	NodeRED libraries	*/
var nodelibrarydir = "/opt/revolt/app/nodes/";

// add node library (file upload)
app.post('/api/add_library',function(req, res){
	console.log("incoming node library");
	var fields = [];
	var form = new formidable.IncomingForm();
 
	form.on('file', function (name, file) {
		console.log(file.path);
		fields[name] = file;
		exec('sh ./scripts/install_nodes.sh '+file.path);
	});

	form.on('end', function () {
		res.writeHead(200, {'content-type': 'text/plain'});
		res.end('{"status" : 200}');
	});

	form.parse(req);
});

// delete library
app.post('/api/remove_library', function (req, res) {
	var library = req.body.library;

	exec('sh ./scripts/remove_nodes.sh '+library, function(err, stdout, stderr){
		console.log(err);
		res.setHeader('Content-Type', 'application/json');
		res.send('{"msg":"library deleted"}');
	});
});

// get list of installed libraries
app.get('/api/library_list', function (req, res) {
	var libraries = fs.readdirSync(nodelibrarydir);
	var list = [];

	for(var key in libraries) {
		list[key] = {};
		list[key].library = libraries[key];
	}

	res.setHeader('Content-Type', 'application/json');
	res.send( JSON.stringify(list) );
});


/*
 *	System
 */

// Firmware update
app.post('/api/firmware_update',function(req, res){
	console.log("incoming firmware package");
	var fields = [];
	var form = new formidable.IncomingForm();
 
	form.on('file', function (name, file) {
		console.log(file.path);
		fields[name] = file;
		exec('sh ./scripts/install_firmware.sh '+file.path);
	});

	form.on('end', function () {
		res.writeHead(200, {'content-type': 'text/plain'});
		res.end('{"status" : 200}');
	});
	form.parse(req);
});

// Download and install firmware
app.get('/api/firmware_downloadAndInstall', function (req, res) {
	console.log("request to download and install firmware");
	res.setHeader('Content-Type', 'application/json');
	res.send({'system':'download and install firmware'});
	exec('sh ./scripts/firmware_downloadAndInstall.sh');
});


// Firmware version
app.get('/api/firmware_version', function (req, res) {
	console.log("request for firmware version");
	res.setHeader('Content-Type', 'application/json');

	system.firmwareVersion( function(data){
		res.send( JSON.stringify(data) );
	});
});

// Hardware platform
app.get('/api/platform', function (req, res) {
	res.setHeader('Content-Type', 'application/json');

	system.hardwarePlatform( function(data){
		res.send( JSON.stringify(data) );
	});
});

// Reboot computer
app.get('/api/reboot', function (req, res) {
	console.log("request for system restart");
	res.setHeader('Content-Type', 'application/json');
	res.send({'system':'reboot'});

	system.restart();
});

// Shutdown computer
app.get('/api/shutdown', function (req, res) {
	console.log("request for system shutdown");
	res.setHeader('Content-Type', 'application/json');
	res.send({'system':'shutdown'});

	system.shutdown();
});

// get system uptime
app.get('/api/uptime', function (req, res) {
	console.log("request for system uptime");
	res.setHeader('Content-Type', 'application/json');

	system.getUptime( function(data){
		res.send(JSON.stringify(data));
	});
});

// get system time
app.get('/api/systime', function (req, res) {
	console.log("request for system time");
	res.setHeader('Content-Type', 'application/json');

	system.getTime( function(data){
		res.send(JSON.stringify(data));
	});
});
