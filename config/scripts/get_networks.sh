#!/bin/bash

#	List wireless networks
#	29/12/2016

# check whether script is run as root
if [ "$(id -u)" != "0" ]; then
   echo "This script must be run as root" 1>&2
   exit 1
fi

# list networks
iw dev wlan0 scan | grep SSID | awk '{ print $2 }'
