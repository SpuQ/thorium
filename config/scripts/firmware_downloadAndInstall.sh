#!/bin/bash

#	firmware_downloadAndInstall.sh
#
#	Download Revolt firmware from master repository (gitlab.com/SpuQ/thorium/) and install.
#	
#	note:	- this script assumes the file is a .tar archieve
#		- this script assumes to run from revolt's system directory
#	TODO this is dirty.


TMPDIR="/tmp/thorium-firmware/"
FIRMWARE="firmware.tar"

mkdir $TMPDIR
cd $TMPDIR

# download archive here
#wget -O $FIRMWARE http://downloads.emcosys.be/revolt-laika/revolt-laika.tar
wget -O $FIRMWARE https://gitlab.com/SpuQ/thorium/repository/archive.tar?ref=master

# untar archive here
tar -xf $FIRMWARE

# go into directory
#cd revolt-core*
cd thorium*

# run uninstall script of previous version, by convension in /opt/revolt/
sh /opt/revolt/uninstall.sh

# install new firmware
bash ./install.sh

# remove temp directory
rm -rf $TMPDIR

# reboot the system
shutdown -r now

exit 0;
