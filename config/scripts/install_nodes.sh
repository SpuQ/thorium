#!/bin/bash

#	install_nodes.sh
#	
#	note:	- this script assumes the file is a .tar archieve
#		- this script assumes to run from revolt's system directory
#	TODO this is dirty?


NODEDIR="/opt/revolt/app/nodes/"
LIBRARY="library.tar"

cd $NODEDIR

# copy archive to node directory
mv $1 $NODEDIR$LIBRARY

# untar archieve here
tar -xf $LIBRARY

# remove archieve
rm -rf $LIBRARY

# TODO install library dependencies if any
#npm install

# TODO restart nodeRED

exit 0;
