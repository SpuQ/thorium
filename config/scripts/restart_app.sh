#!/bin/bash

#	Restart NodeRED
#	requires the node-red script in /etc/init.d/
#

/etc/init.d/node-red stop
/etc/init.d/node-red start &
exit 0;
