#!/bin/bash

#	install_firmware
#	
#	note:	- this script assumes the file is a .tar archieve
#		- this script assumes to run from revolt's system directory
#	TODO this is dirty?


TMPDIR="/tmp/revolt-firmware/"
FIRMWARE="firmware.tar"

mkdir $TMPDIR
cd $TMPDIR

# copy archive to application ui directory
mv $1 $TMPDIR$FIRMWARE

# untar archive here
tar -xf $FIRMWARE

# run uninstall script of previous version, by convension in /opt/revolt/
sh /opt/revolt/uninstall.sh

# install new firmware
bash ./install.sh

# remove temp directory
rm -rf $TMPDIR

# reboot the system
shutdown -r now

exit 0;
