#!/bin/bash

# TODO cleaner!


# check whether script is run as root
if [ "$(id -u)" != "0" ]; then
   echo "This script must be run as root" 1>&2
   exit 1
fi

HOSTAPDFILE="/etc/hostapd/hostapd.conf"
HOSTAPDDEFAULT="/etc/default/hostapd"
DHCPFILE="/etc/dhcp/dhcpd.conf"
DHCPDEFAULT="/etc/default/isc-dhcp-server"


clearHostapd() {
	rm $HOSTAPDFILE
	rm $HOSTAPDDEFAULT
}

clearDHCP() {
	rm $DHCPFILE
	rm $DHCPDEFAULT
}

service hostapd stop
service isc-dhcp-server stop

clearDHCP
clearHostapd

killall hostapd

exit 0;
