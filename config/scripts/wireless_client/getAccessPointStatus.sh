#!/bin/bash

#	check wireless Access Point status
#	02/02/2017
#

# check whether script is run as root
if [ "$(id -u)" != "0" ]; then
   echo "This script must be run as root" 1>&2
   exit 1
fi

if [ -z "$1" ]; then
	echo "first argument must be a wireless interface"
	exit 1;
fi

INTERFACE=$1
ADDRESS=$(ifconfig $INTERFACE | grep 'inet addr:' | cut -d: -f2 | awk '{ print $1}');

#TODO check for hostapd process?

printf "active\n"
printf "$ADDRESS"

exit 0
