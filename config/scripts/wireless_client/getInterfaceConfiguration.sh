#!/bin/bash

#	List wireless networks
#	29/12/2016

# check whether script is run as root
if [ "$(id -u)" != "0" ]; then
   echo "This script must be run as root" 1>&2
   exit 1
fi

# check for interface in argument
if [ -z "$1" ]; then
	echo "first argument must be a wireless interface"
	exit 1;
fi

ifconfig_line=$(ifconfig $1 | grep -sw "inet" | tr ":" " ")

echo $(echo $ifconfig_line | awk {'print $3'})
echo $(echo $ifconfig_line | awk {'print $7'})
echo $(route -n |head -n3|tail -n1|awk '{print $2}')
