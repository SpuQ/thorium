#!/bin/bash

#	check wireless client status
#	08/01/2017
#

# check whether script is run as root
if [ "$(id -u)" != "0" ]; then
   echo "This script must be run as root" 1>&2
   exit 1
fi

SSID=$(iwgetid);
#$SSID="$(iwconfig | grep wlan0)";

if [ -n "$SSID" ]
then
	echo "connected";
else
	echo "disconnected";
fi


exit 0
