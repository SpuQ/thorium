var url = document.location.href;

$( document ).ready(function() {
	url = "http://"+document.location.host+"/";
	init();
	setInterval(timedRefresh, 1000);
});

// initialize interface
function init(){
	open_networkSettings();		// open network settings

	/* devices */
	getDeviceList();		// initialize device list
	
	/* networking */
	get_networking_settings();	// initialize networking settings
	get_wlan_client_settings();	// initialize wlan client settings
	get_wlan_ap_settings();		// initialize Wlan access point settings

	/* system */
	getFirmwareVersion();		// initialize firmware version
	getUptime();			// initialize uptime
	getPlatform();			// initialize platform
}

// timed updates
function timedRefresh(){
	// only refresh values for opened tab
	// to unburden the host

	if(tab=="network"){
		get_wlan_client_status();	// refresh wlan client status
		get_wlan_ap_status();		// refresh wlan access point status
	}
	else if(tab=="application"){
		getDeviceList();		// refresh device list
		getLibrarylist();
	}
	else if(tab=="system"){
		getUptime();			// refresh uptime
		getSystime();			// get system time
	}
	else if(tab=="account"){

	}
}


/*
 *	Menu
 */
var tab;

// menu buttons
var menu_network = document.getElementById('menu-network');
var menu_application = document.getElementById('menu-application');
var menu_system = document.getElementById('menu-system');

menu_network.onclick = open_networkSettings;
menu_application.onclick = open_applicationSettings;
menu_system.onclick = open_systemSettings;

// divs pages
var settings_network = document.getElementById('network-settings');
var settings_application = document.getElementById('application-settings');
var settings_system = document.getElementById('system-settings');

function open_networkSettings(){
	tab = "network";

	settings_network.style = "display: block;";
	settings_application.style = "display: none;";
	settings_system.style = "display: none;";

	$('#menu-network').addClass("active");
	$('#menu-application').removeClass("active");
	$('#menu-system').removeClass("active");

	// initialize tab
	get_available_networks();
}

function open_applicationSettings(){
	tab = "application";

	settings_network.style = "display: none;";
	settings_application.style = "display: block;";
	settings_system.style = "display: none;";

	$('#menu-network').removeClass("active");
	$('#menu-application').addClass("active");
	$('#menu-system').removeClass("active");

	// initialize tab
	getLibrarylist();	// get list of installed libraries
}

function open_systemSettings(){
	tab = "system";

	settings_network.style = "display: none;";
	settings_application.style = "display: none;";
	settings_system.style = "display: block;";

	$('#menu-network').removeClass("active");
	$('#menu-application').removeClass("active");
	$('#menu-system').addClass("active");
}


/*
 *	Networking settings
 */
// get networking settings
function get_networking_settings(){
	$.get("/api/networking/networking_settings", function(data){
		console.log("json data: "+JSON.stringify(data));
		console.log("networking settings:\nWAP enabled: "+data.wap_enabled+"\nWAP if network is unavailable: "+data.wap_if_unavailable);
		$('#networking_wap_enabled').prop('checked', data.wap_enabled);
		$('#networking_wap_if_unavailable').prop('checked',data.wap_if_unavailable);
	}, 'json');
}

// save networking settings
function save_networking_settings(){
	var data = {};

	data.wap_enabled = $('#networking_wap_enabled').prop('checked');
	data.wap_if_unavailable = $('#networking_wap_if_unavailable').prop('checked');

	console.log("networking settings:\nWAP enabled: "+data.wap_enabled+"\nWAP if network is unavailable: "+data.wap_if_unavailable);
	console.log("json data: "+JSON.stringify(data));

	// "boolean bug" - explicitly set dataType and contentType! #DuctapeFix
	$.ajax({
		url: url+'api/networking/networking_settings',
		type: 'POST',
		data: JSON.stringify(data),
		contentType: 'application/json; charset=utf-8',
		dataType: 'json',
		async: false,
		success: function(msg) {
			notify(msg);
		}
	});
	/*
	$.post("/api/networking/networking_settings", data, function(res){
		notify(res.msg);
	}, 'json'); // "boolean bug" set datatype! (otherwise boolean value becomes string)
	*/
}

// get wireless client status
function get_wlan_client_status(){
	$.get("/api/networking/wclient_status", function(data){
		$('#wc_status').val(data.status);
		$('#wc_substatus').val(data.substatus);
	});
}

// get WLAN client settings
function get_wlan_client_settings(){
	$.get("/api/networking/wclient_settings", function(data){
		console.log("wireless client settings:\nssid: "+data.ssid+"\nsecurity: "+data.security+"\npassword: "+data.passwd+"\nstatic: "+data.static+"\naddress: "+data.address+"\nnetmask: "+data.netmask );

		if(typeof data.ssid != "undefined") $('#wc_ssid').val( data.ssid );
		if(typeof data.security != "undefined") $('#wc_security').val( data.security );
		if(typeof data.passwd != "undefined") $('#wc_passwd').val( data.passwd );
		if(typeof data.static != "undefined") $('#wc_static').prop('checked', data.static);
		if(typeof data.address != "undefined") $('#wc_address').val( data.address );
		if(typeof data.netmask != "undefined") $('#wc_netmask').val( data.netmask );
	}, 'json');
}

// save WLAN client settings
function save_wlan_client_settings(){
	var data = {};

	data.ssid = $('#wc_ssid').val();
	data.security = $('#wc_security').val();
	data.passwd = $('#wc_passwd').val();
	data.static = $('#wc_static').prop('checked');
	data.address = $('#wc_address').val();
	data.netmask = $('#wc_netmask').val();


	console.log("wireless client settings:\nssid: "+data.ssid+"\nsecurity: "+data.security+"\npassword: "+data.passwd+"\nstatic: "+data.static+"\naddress: "+data.address+"\nnetmask: "+data.netmask);

	// "boolean bug" - explicitly set dataType and contentType!
	$.ajax({
		url: url+'api/networking/wclient_settings',
		type: 'POST',
		data: JSON.stringify(data),
		contentType: 'application/json; charset=utf-8',
		dataType: 'json',
		async: false,
		success: function(msg) {
			notify(msg);
		}
	});
	/*
	$.post("/api/networking/wclient_settings", data, function(res){
		notify(res.msg);
	}, 'json');
	*/
}

// get list of available wireless networks
var available_networks_list;

function get_available_networks(){
	$.get("/api/networking/available_networks", function(data){
		available_networks_list = data;
		display_available_networks_list(data);
	});
}

function display_available_networks_list(networks){
	$("#wc_available_networks").empty();	// empty list

	for(var i in networks){
		$("#wc_available_networks").append('<option value="'+networks[i].ssid+'">'+networks[i].ssid+'</option>');
	}
}


// get WLAN access point settings
function get_wlan_ap_settings(){
	$.get("/api/networking/wap_settings", function(data){
		console.log("access point settings:\nssid: "+data.ssid+"\npassword: "+data.password+"\nsecurity: "+data.security);

		if(typeof data.ssid != "undefined") $('#wap_ssid').val( data.ssid );
		if(typeof data.security != "undefined") $('#wap_security').val( data.security );
		if(typeof data.passwd != "undefined") $('#wap_passwd').val( data.passwd );
	});
}

// get WLAN acces point status
function get_wlan_ap_status(){
	$.get("/api/networking/wap_status", function(data){
		if(typeof data.status != "undefined") $('#wap_status').val(data.status);
		if(typeof data.substatus != "undefined") $('#wap_substatus').val(data.substatus);
	});
}

// save WLAN access point settings
function save_wlan_ap_settings(){
	var data = {};

	data.ssid = $('#wap_ssid').val();
	data.passwd = $('#wap_passwd').val();
	data.security = $('#wap_security').val();

	console.log("access point settings:\nssid: "+data.ssid+"\npassword: "+data.password+"\nsecurity: "+data.security);

	$.post("/api/networking/wap_settings", data, function(res){
		notify(res.msg);
	});
}


/*
 *	Device list
 */
var devicelist;

function display_elements(objects, items){
	$("#devicelist").empty();

	for(var i in objects){
		$("#devicelist").append('<li class="list-group-item" onClick="deviceClick('+i+');">'+objects[i].device+'</li>');
	}
}

function deviceClick(index){
	notify(""+devicelist[index].device+"<br>Feature not available yet" );
}

function getDeviceList(){
	$.get("/api/qdevice-list", function(data){
		devicelist = data;
		display_elements(data, 100);
	});
}


/*
 *	Application functions
 */
function applicationRestart(){
	$.get("/api/restart_app", function(data){
		console.log(data);
		notify("Application is restarting");
	});
}

function openProgramEditor(){
	dialog("Warranty notification", 'Opening the program editor will void your warranty. We\'re just saying. If you understand what you\'re doing, we strongly encourage you to proceed!<br><br><i>\"Hacking is not authorized, and not illegal\"<br>- Rodney Mullen @ TEDx</i><br>', function(){
		var editor = "http://"+document.location.hostname + ":1880";
		window.open(editor);
	});
}

// delete flow
function deleteProgram(){
	dialog("Delete Program", 'Deleting the program... yeah... removes all functionality of the application. This dialog is mainly a protection to catch a missed click.', function(){
		$.get("/api/program_delete", function(data){
			notify("Program deleted");
		});
	});
}

function openWebUI(){
	var webui = "http://"+document.location.hostname+":80/";
	window.open(webui);
}

/*	add node library	*/
function uploadLibrary(){
	dialog("Add library","blabla terms and conditions", function(){
		openBusy("Uploading library");
		console.log("Uploading Library\nURL: "+url);

		var myFormData = new FormData();
		myFormData.append('libraryFile', $('#libraryFile')[0].files[0]);
	
		$.ajax({
			url: url+'api/add_library',
			type: 'POST',
			processData: false, // important
			contentType: false, // important
			dataType : 'json',
			data: myFormData,
			success: function( msg){
				console.log("Done uploading library!");
				closeBusy();
				getLibrarylist();
			}      
		});
	});
}

// for new file-upload layout
$('#libraryFile').change(function(){
	$('#library-upload-subfile').val($(this).val().split('\\').pop());
});

/*	node library list	*/
// TODO dirty
var librarylist;

function display_libraries(objects){
	var i;

	$("#libraries").empty();

	for(var i in objects){
		$("#libraries").append('<li class="list-group-item" onClick="libraryClick('+i+');">'+objects[i].library+'</li>');
	}
}

function libraryClick(index){
	dialog("delete "+librarylist[index].library+" library","Deleting a library may lead to malfunctioning of the application. Continue if you understand the risks.", function(){ 
	var data = {};
	data.library = librarylist[index].library;

	$.post('/api/remove_library', data, function(){ 
			notify("library deleted");
			getLibrarylist();
		});
	});
}

function getLibrarylist(){
	$.get("/api/library_list", function(data){
		librarylist = data;
		display_libraries(data);
	});
}


/*
 *	Firmware
 */

function uploadFirmware(){
	dialog("Update firmware","Updating the Revolt firmware will delete all current files, including custom UI, program, ... .<br>The system will reboot during the installation process.<br><br><i><b>Note</b><br>We recommend to only use firmware packages from trusted sources, because faulty software may cause harm to this system. When using non-official firmware, your warranty is void.</i>", function(){

		console.log("Uploading Firmware\nURL: "+url);
		openBusy("Uploading firmware");

		var myFormData = new FormData();
		myFormData.append('firmwareFile', $('#firmwareFile')[0].files[0]);
		console.log("file name: "+$('#firmwareFile')[0].files[0].name );
	
		$.ajax({
			url: url+'api/firmware_update',
			type: 'POST',
			processData: false, // important
			contentType: false, // important
			dataType : 'json',
			data: myFormData,
			success: function( msg){
				console.log("Done uploading Firmware!");
				openBusy("Installing firmware, this may take a minute...");
			}      
		});
	});
}


$('#firmwareFile').change(function(){
	$('#firmware-upload-subfile').val($(this).val().split('\\').pop());
});

// download and install firmware
var firmware_downloadAndInstall = document.getElementById('firmware_downloadAndInstall');
firmware_downloadAndInstall.onclick = function(){
	dialog("Download & install","Updating the Revolt firmware will delete all current files, including custom UI, program, ... .<br>The system will reboot during the installation process.<br>Make sure the Revolt computer is connected to the internet<br><br><input type='checkbox' disabled=true>keep system settings<br><input type='checkbox' disabled=true>keep application", function(){

		$.get("/api/firmware_downloadAndInstall", function(data){
			console.log(data);
			openBusy("Downloading and installing firmware...");
		});
	});
}


/*
 *	System functions
 */

function systemRestart(){
	$.get("/api/reboot", function(data){
		console.log(JSON.stringify(data));
		openBusy("System is restarting, this may take a minute");
	});
}

function systemShutdown(){
	dialog("Revolt System", "This system is about to shut down. Note that from this point on the system will require physical interaction with the Revolt computer to restart.", function(){
		$.get("/api/shutdown", function(data){
			console.log(data);
			openBusy("System is shutting down");
		});
	});
}

function getPlatform(){
	$.get("/api/platform", function(data){
		$('#platform').val(data.platform);
	});
}

function getUptime(){
	$.get("/api/uptime", function(data){
		$('#uptime').val(data.uptime);
	});
}

function getSystime(){
	$.get("/api/systime", function(data){
		$('#systime').val(data.systime);
	});
}

function getFirmwareVersion(){
	$.get("/api/firmware_version", function(data){
		$('#firmwareversion').val(data.firmware);
	});
}


/*
 *	Dialog
 */
function dialog( title, msg , action){
	$('#dialog').modal('show');

	$('#dialog-title').html(title);
	$('#dialog-text').html(msg);

	$('#dialog-continue').click(function() {
		action();
		$('#dialog-continue').unbind();		// remove event handler
	});
}


/*
 *	Notification
 */
function notify(msg){
	$('#notification').modal('show');

	$('#notification-text').html(msg);
}

/*
 *	Busy
 */
function openBusy(msg){
	if(typeof msg != "undefined"){
		$('#busy-text').html(msg);
	}

	$('#busy').modal('show');
}

function closeBusy(msg){
	$('#busy').modal('hide');
}

/*
 *	Application webUI
 */
function uploadWebUI(){
	console.log("Uploading WebUI\nURL: "+url);
	openBusy("Uploading WebUI");
	var myFormData = new FormData();
	myFormData.append('webuiFile', $('#webuiFile')[0].files[0]);
	
	$.ajax({
		url: url+'api/uploadWebui',
		type: 'POST',
		processData: false, // important
		contentType: false, // important
		dataType : 'json',
		data: myFormData,
		success: function( msg){
			console.log("Done uploading WebUI!");
			closeBusy();
		}      
	});
}


$('#webuiFile').change(function(){
	$('#webui-upload-subfile').val($(this).val().split('\\').pop());
});
